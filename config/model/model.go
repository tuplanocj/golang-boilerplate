package model

// Settings contains all configuration struct
type Settings struct {
	Database   Database
	ClientGRPC ClientGRPC
}

// Database structure
type Database struct {
	Username string
	Port     string
	Host     string
	Password string
	Database string
}

type ClientGRPC struct {
	Host string
	Port string
}
