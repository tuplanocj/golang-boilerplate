package config

import (
	"golang-boilerplate/config/model"
	"strings"
)

var (
	settings model.Settings
	env      = "dev"
)

func Configuration() model.Settings {

	switch strings.ToLower(env) {
	case "dev":
		settings = model.Settings{
			Database: model.Database{
				Username: "root",
				Port:     "3306",
				Host:     "127.0.0.1",
				Password: "root",
				Database: "db_name",
			},
			ClientGRPC: model.ClientGRPC{
				Host: "127.0.0.1",
				Port: "7070",
			},
		}
	}

	return settings
}
