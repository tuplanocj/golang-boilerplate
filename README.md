
# README #

Go is an open source programming language that makes it easy to build simple, reliable, and efficient software.

### What is this repository for? ###

* This is just a simple boilerplate for golang.
* It includes the following:
  - configuration setup
  - database connection
  - grpc connections
  - data struct creation

### Folder Structure ###
  - config
  - modules
    - client (module name)
      - connection (contains all connections like db, grpc, redis, mq and etc.,)
        - database
        - grpc
      - dao (contains database query per module)
      - model (contains data structures)
      - proto (contains proto file per module)
      - services (contains helper/services for a specific module)
      - main.go 

### How to setup ? ###

* I created 2 sample modules, client and hello-world module. To run the services, please do the bellow commands
hello-world - as a server
client -  as a client to request from server

* Start the server (hello-world)
cd modules/hello-world
go run main.go

* After running the server, you can send request to the server using the client service.

cd modules/client
go run main.go

### References ### 

* References to check:
  * GRPC - https://grpc.io/docs/languages/go/
  * For simple Golang Framework: https://godoc.org/gopkg.in/iris-framework/iris.v6
  * For Go micro implementation
    - Please refer to this tutorial : https://itnext.io/micro-in-action-getting-started-a79916ae3cac


### Generate proto file
protoc -I routeguide/ routeguide/route_guide.proto --go_out=plugins=grpc:routeguide