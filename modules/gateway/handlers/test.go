package handlers

import (
	"context"
	"log"
	"time"

	clientgrpc "golang-boilerplate/modules/client/connection/grpc"
	greeterProto "golang-boilerplate/modules/client/proto"

	"github.com/kataras/iris"
)

// serves as controller

func Sample(ctx iris.Context) {
	// params := model.Params{}
	// ctx.ReadJSON(&params)

	// sampleString := `{
	// 	"name": "Joy",
	// 	"age": 24,
	// 	"address": "jibeeee"
	// }`

	// paramsTest := model.Params{}
	// json.Unmarshal([]byte(sampleString), &paramsTest)

	// // struct to byte
	// paramsTestByte, _ := json.Marshal(paramsTest)
	// log.Println(string(paramsTestByte))

	// ctx.JSON(paramsTest)
	// return

	conn := clientgrpc.GRPCConnect()

	client := greeterProto.NewGreeterClient(conn)
	defer conn.Close()

	ctx2, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(60))
	defer cancel()

	res, err := client.PrintGreetings(ctx2, &greeterProto.GreetingRequest{
		Name: "Joy",
	})

	if err != nil {
		log.Println(err)
		return
	}

	ctx.JSON(res)
}
