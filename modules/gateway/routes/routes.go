package routes

import (
	test "golang-boilerplate/modules/gateway/handlers"

	"github.com/iris-contrib/middleware/cors"
	"github.com/kataras/iris"
)

//Routes to be called in main.go
func Routes() {

	app := iris.New()
	crs := cors.New(cors.Options{
		AllowedOrigins:   []string{"*"}, // allows everything, use that to change the hosts.
		AllowCredentials: true,
		AllowedMethods:   []string{"GET", "POST", "OPTIONS"},
		AllowedHeaders:   []string{"*"},
	})

	app.Use(crs)
	app.AllowMethods(iris.MethodOptions)
	//routes
	app.Post("/test", test.Sample, crs)

	app.Run(iris.Addr(":8090"), iris.WithoutServerError(iris.ErrServerClosed))
}
