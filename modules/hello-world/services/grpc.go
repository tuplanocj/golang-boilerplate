package services

import (
	"context"
	greeterProto "golang-boilerplate/modules/hello-world/proto"
	"log"
)

type Server struct{}

func (s *Server) PrintGreetings(ctx context.Context, reqData *greeterProto.GreetingRequest) (*greeterProto.GreetingResponse, error) {
	name := reqData.Name

	log.Println("Received Request from client")

	return &greeterProto.GreetingResponse{
		Name: name,
		Text: "Hello " + name,
	}, nil
}
