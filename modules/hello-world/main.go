package main

import (
	"golang-boilerplate/modules/hello-world/services"
	"log"
	"net"

	greeterProto "golang-boilerplate/modules/hello-world/proto"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

var server = services.Server{}

func main() {
	forever := make(chan bool)

	conn, err := net.Listen("tcp", ":7070")
	if err != nil {
		// displays error
		log.Println("Greeter GRPC Error: ", err.Error())
	} else {
		log.Println("Greeter GRPC Started ...")
	}
	//create grpc server
	s := grpc.NewServer()
	greeterProto.RegisterGreeterServer(s, &server)
	//Register refelection service on gRPC server
	reflection.Register(s)
	if err := s.Serve(conn); err != nil {
		log.Println("Greeter GRPC server error")
	}

	<-forever
}
