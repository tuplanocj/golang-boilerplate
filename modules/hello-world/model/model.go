package model

// GreetingsResponse response structure
type GreetingsResponse struct {
	Name string `json:"name,omitempty"` // omitempty is used to eliminate field if empty/null
	Text string `json:"text,omitempty"`
}

type GreetingsRequest struct {
	Name     string `json:"name,omitempty"` // omitempty is used to eliminate field if empty/null
	Age      int
	Nickname []string `json:"nicknames"`
	Address  *Address `json:"address,omitempty"`
	Text     struct {
	} `json:"text,omitempty"`
}

type Address struct {
	City    string `json:"city,omitempty"`
	Country string `json:"country,omitempty"`
}
