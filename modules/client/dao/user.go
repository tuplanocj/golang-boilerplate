package dao

import (
	db "golang-boilerplate/modules/hello-world/connection/database"
	"golang-boilerplate/modules/hello-world/model"
)

// GetClients returns list of clients
func GetClients() []model.Client {
	var clients []model.Client
	err := db.Gorm.Debug().Raw(`SELECT * FROM client`).Error
	if err != nil {
		log.Println(err)
	}
	return clients
}
