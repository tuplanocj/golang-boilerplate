package db

import (
	"golang-boilerplate/config"
	"log"

	"github.com/jinzhu/gorm"
)

var (
	settings = config.Configuration()
	Gorm     *gorm.DB
)

func gormDBConnection() {
	db, err := gorm.Open("mysql", settings.Database.Username+":"+
		settings.Database.Password+"@tcp("+settings.Database.Host+":"+settings.Database.Port+")/"+
		settings.Database.Database+"?charset=utf8mb4&parseTime=True&loc=Local")

	db.SingularTable(true)
	db.LogMode(false)

	Gorm = db

	if err != nil {
		log.Println("[X] " + err.Error())
	}
}

func init() {
	gormDBConnection()
}
