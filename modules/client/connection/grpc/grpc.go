package clientgrpc

import (
	"golang-boilerplate/config"
	configmodel "golang-boilerplate/config/model"
	"log"

	"google.golang.org/grpc"
)

var settings configmodel.Settings

func GRPCConnect() *grpc.ClientConn {
	address := settings.ClientGRPC.Host + ":" + settings.ClientGRPC.Port
	// Set up a connection to the server
	conn, err := grpc.Dial(address, grpc.WithInsecure())
	if err != nil {
		log.Println(err)
		return GRPCConnect()
	}

	return conn
}

func init() {
	settings = config.Configuration()
}
