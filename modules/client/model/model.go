package model

// GreetingsResponse response structure
type GreetingsResponse struct {
	Name string `json:"name,omitempty"` // omitempty is used to eliminate field if empty/null
	Text string `json:"text,omitempty"`
}

type GreetingsRequest struct {
	Name string `json:"name,omitempty"` // omitempty is used to eliminate field if empty/null
	Age  string `json:"age,omitempty"`
}
