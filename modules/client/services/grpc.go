package services

import (
	"context"
	greeterProto "golang-boilerplate/modules/client/proto"
)

type Server struct{}

func (s *Server) PrintGreetings(ctx context.Context, reqData *greeterProto.GreetingRequest) (*greeterProto.GreetingResponse, error) {
	name := reqData.Name

	return &greeterProto.GreetingResponse{
		Name: name,
		Text: "Hello " + name,
	}, nil
}
