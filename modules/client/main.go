package main

import (
	"context"
	clientgrpc "golang-boilerplate/modules/client/connection/grpc"
	greeterProto "golang-boilerplate/modules/client/proto"
	"golang-boilerplate/modules/client/services"
	"log"
	"time"
)

var server = services.Server{}

func main() {
	conn := clientgrpc.GRPCConnect()

	client := greeterProto.NewGreeterClient(conn)
	defer conn.Close()

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(60))
	defer cancel()

	res, err := client.PrintGreetings(ctx, &greeterProto.GreetingRequest{
		Name: "Joy",
	})

	if err != nil {
		log.Println(err)
		return
	}

	log.Println(res)

}
